Inline Block
==================================

Requirements:
block
entity
ctools
context
blockreference

Recommended:
Bean

Use-Case:
Use Inline Block to allow your content editors to place blocks on individual
nodes, per content-type. This module works best when used in conjunction with
the Bean module. Since the bean module also allows content-editors to create
their own blocks, it is extremely useful to allow the content-editor to place
it in the regions they desire.

How-To Use:
Inline Blocks are used on a per-content-type and per-region basis. In order to
enable an inline block, you must first visit the permissions page to assign the
permission to use it for a particular theme/region combination. However, this
is only half of the setup. The other half is to visit the content-type edit page
to set up the specific content-types you want to use inline blocks with. Each
content-type is also set up on a region-basis. This means that, in order for a
content-editor to be able to place an inline block, the content-editor must
have permission *and* be editing a content-type enabled to use inline blocks.

It is strongly suggested that you use a "dedicated region" for your inline
blocks. A "dedicated region" is a region that has its blocks populated only by
inline blocks. Therefore when trying to determine how a block is being placed
on a particular page you will know that only inline blocks is placing it there.
You can use either an unused region provided by your theme, or create a new one
if necessary.


Permissions are handled on a per-region basis. This module will scan the
regions file to determine if the logged-in user has the right to add a block to
any region. Only the allowed regions will be shown. If the user has no rights,
nothing will be added to the node edit form.

This module works by using its own Context plugin.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
The following software is provided as noted in the terms of the copyright
notice.

The purpose of this software is:   A new module to allow the insertion of blocks
into individual nodes by the content-editors.

This software is being submitted by Argonne National Laboratory, January 10,
2013.

The authors of this improvement are:
    Jason Partyka (partyka - http://drupal.org/user/344048)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
