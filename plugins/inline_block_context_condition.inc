<?php
/**
 * @file
 * Implements a context condition for the Inline Blocks module.
 */

/**
 * Provides an inline block condition to context. No UI for this context.
 */
class inline_block_context_condition extends context_condition {
  /**
   * Execute.
   */
  function execute() {
    if ($this->condition_used()) {
      // Include both the path alias and normal path for matching.
      $current_path = explode("/", current_path());
      if ($current_path[0] != "node") {
        // If this is not a node, we don't care.
        return;
      }
      $nid = $current_path[1];
      if (!is_numeric($nid)) {
        // This is not a node page, but something else about nodes.
        return;
      }
      $vid = NULL;
      if (count($current_path) > 4 && ($current_path[2] == 'revisions' && $current_path[4] == 'view')) {
        $vid = $current_path[3];
      }
      elseif (count($current_path) > 2) {
        // Condition not met if there's a sub-path and it's not to
        // view the revision.
        return;
      }

      if ($this->findInlineFields($nid, $vid)) {
        // There should only be one condition object
        // we're simply programatically determining if this
        // condition happens to fit.
        foreach ($this->get_contexts('inline_block_context_condition') as $context) {
          $this->condition_met($context);
        }
      }
    }
  }


  /**
   * Determine if a node has inline blocks assigned to it.
   *
   * @param int $nid
   *   The (entity id) to find if there are inline blocks for.
   */
  private function findInlineFields($nid, $vid = NULL) {
    // First, let's determine the node type.
    $sql = "SELECT * FROM {node} n WHERE n.nid = :nid LIMIT 1";
    $result = db_query($sql, array(":nid" => $nid));
    foreach ($result as $record) {
      $node = $record;
      // Most likely not necessary, just being defensive.
      break;
    }

    // Now, we need to determine if any inline blocks
    // exist. we don't really care what is in the rows selected
    // by the statement at this point, just that the fields
    // exist.
    $sql = "SELECT id, field_id, field_name, entity_type FROM {field_config_instance} fci WHERE
      fci.bundle = :bundle AND fci.field_name like 'ib_%' AND
      fci.entity_type = 'node'";

    $result = db_query($sql, array(":bundle" => $node->type));

    $blocks_exist = FALSE;
    foreach ($result as $record) {
      // Now that we know that fields exist, let's see if
      // any of these fields have data in them. if they do,
      // then the condition is met.
      $table = "field_data_{$record->field_name}";
      if ($vid != NULL) {
        $sql = "SELECT * FROM {{$table}} t WHERE t.entity_id = :nid LIMIT 1";
        $result = db_query($sql, array(":nid" => $nid));
      }
      else {
        $sql = "SELECT * FROM {{$table}} t WHERE t.entity_id = :nid AND t.revision_id = :revision_id LIMIT 1";
        $result = db_query($sql, array(":nid" => $nid, ":revision_id" => $vid));
      }
      $record = $result->fetchObject();
      if ($record != NULL) {
        $blocks_exist = TRUE;
        break;
      }
    }
    return $blocks_exist;
  }

  /**
   * We always need to evaluate this condition if the module is enabled.
   *
   * @return bool TRUE
   */
  function condition_used() {
    return TRUE;
  }
}
