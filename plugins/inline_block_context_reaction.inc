<?php
/**
 * @file
 * Implements a context reaction for the Inline Blocks module. Based upon
 * code found in context/plugins/context_reaction_block.inc.
 */

class inline_block_context_reaction extends context_reaction {

  /**
   * This reaction has no settings.
   *
   * @param array $context
   *   A drupal form array
   *
   * @return array
   *   Array indicating this reaction has no settings.
   */
  function options_form($context) {
    return array(
      '#title' => "Inline Blocks",
      '#description' => "There are no settings for this reaction.",
    );
  }

  /**
   * This should be the only interface method needed because we have no UI.
   *
   * @param array $page
   *   Passed in by context.
   */
  function execute(&$page) {
    global $theme;

    // The theme system might not yet be initialized. We need $theme.
    drupal_theme_initialize();

    // Populate all block regions.
    $all_regions = system_region_list($theme);

    // Load all region content assigned via blocks.
    foreach (array_keys($all_regions) as $region) {
      if ($blocks = $this->blocks_in_region($region)) {
        // Merges context-assigned blocks into what's already defined.
        $page[$region] = isset($page[$region]) ? array_merge($page[$region], $blocks) : $blocks;
      }
    }
  }

  /**
   * Determines what blocks have been assigned to a region.
   *
   * @param string $region
   *   The machine name of a valid Drupal region.
   *
   * @return array
   *   The blocks assigned to a particular region.
   */
  private function blocks_in_region($region) {
    global $theme;

    $list = array();
    $build = array();

    // Both $theme and $region should be 'safe' here (as the table name) because
    // they are provided to this class by drupal and not by any input from the
    // user. Additionally, we are using Drupal's table name substitution
    // capability, therefore, a query should not be executed if the table
    // isn't known to Drupal.
    $field = "ib_{$theme}_{$region}_bid";
    $current_path = explode("/", current_path());
    if (count($current_path) <= 1) {
      // We have nothing to worry about.
      return NULL;
    }
    $nid = $current_path[1];
    $vid = NULL;

    if (count($current_path) > 4 &&
        ($current_path[2] == 'revisions' && $current_path[4] == 'view')) {
      $vid = $current_path[3];
    }
    elseif (count($current_path) > 2) {
      // Condition not met if there's a sub-path and it's not to view the
      // revision.
      // This should be caught by the condition object, but in case it's not
      // we're double-checking.
      return $build;
    }
    $schema = drupal_get_schema();

    foreach ($schema as $key => $table) {
      // We're looking to see if the table name starts with "field_data_ib_".
      // If it starts with those characters, then we know we have a field for
      // that region.
      $field_data_table = "field_data_ib_{$theme}_{$region}";
      $field_revision_table = "field_revision_ib_{$theme}_{$region}";
      $haystack = drupal_substr($key, 0, drupal_strlen($field_data_table));
      if ($field_data_table != $haystack) {
        // No match, move on to the next.
        continue;
      }

      // Check to see if the $region passed in matches the
      // the region embedded in the table name. need to do this because
      // there could be some partial matches.
      if ($key != $field_data_table) {
        continue;
      }

      if ($vid == NULL) {
        $sql = "SELECT {$field} as bid, delta as delta FROM {{$field_data_table}} where entity_id = :nid";
        $result = db_query($sql, array(":nid" => $nid));
      }
      else {
        $sql = "SELECT {$field} as bid, delta as delta FROM
          {{$field_revision_table}} where entity_id = :nid AND
          revision_id = :revision_id";
        $result = db_query($sql, array(":nid" => $nid, ":revision_id" => $vid));
      }

      foreach ($result as $record) {
        $sql = "SELECT * FROM {block} b where b.bid = :bid";
        $block_result = db_query($sql, array(":bid" => $record->bid));
        $block_record = $block_result->fetchObject();
        $list_temp = array(
          "module" => $block_record->module,
          "delta" => $block_record->delta,
          "region" => $region,
          "weight" => $record->delta,
          "context" => "inline-block-{$theme}-{$region}",
          "bid" => "{$block_record->module}-{$block_record->delta}",
          "title" => isset($block_record->title) ? $block_record->title : NULL,
          "cache" => isset($block_record->cache) ? $block_record->cache : DRUPAL_NO_CACHE,
        );

        // Adding this block into the block list.
        $list["{$block_record->module}_{$block_record->delta}"] = (object) $list_temp;

      }

      break;
    }

    // At this point, if there are items in the list object,
    // then we have blocks to render.
    if (count($list) > 0) {
      module_load_include('module', 'block', 'block');
      $list = _block_render_blocks($list);
      $build = _block_get_renderable_array($list);
    }

    return $build;

  }

}
